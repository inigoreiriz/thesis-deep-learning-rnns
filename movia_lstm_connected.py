from lstm_common import *

import pandas as pd
import numpy as np

def data_preprocessing():

    data = pd.read_csv('../data/4A_201701_Consistent.csv', sep=';')

    # Initial data-slicing
    data = data[(data.LinkTravelTime > 0) & (data.LineDirectionCode == 1)]

    # Data convertion
    data['DateTime'] = pd.to_datetime(data['DateTime'])
    time = pd.DatetimeIndex(data['DateTime'])
    data['TimeOfDayClass'] = 'NO_PEEK'
    data['Hour'] = time.hour
    data.ix[((7 < time.hour) & (time.hour < 9) & (
        data['DayType'] == 1)), 'TimeOfDayClass'] = 'PEEK'
    data.ix[((15 < time.hour) & (time.hour < 17) & (
        data['DayType'] == 1)), 'TimeOfDayClass'] = 'PEEK'

    data = data[(26 <= data.LineDirectionLinkOrder) &
                (data.LineDirectionLinkOrder <= 32)]

    grouping = data.groupby(['LinkRef'])

    return grouping

def generate_features(data):

    data = data.sort_values("DateTime", ascending=False)
    data = data.set_index(np.arange(0, data.shape[0], 1))

    num_lags = 20
    target = []
    lags = []
    for i in range(data.shape[0]):
        target.append(data.iloc[i].LinkTravelTime)
        lags.append(data.LinkTravelTime.shift(-1)[i:i + num_lags].values)
    mat = pd.DataFrame(lags)
    mat = mat.dropna(axis=0)

    mat = np.array(mat)
    X = mat[0:5000, :]
    y = np.array(target[0:5000]).reshape(-1, 1)

    assert X.shape[0] == y.shape[0]

    X = np.reshape(X, (X.shape[0], X.shape[1], 1))

    return X, y

def split_into_train_test(X, y):

    X_train, X_test = np.split(X, [int(.8 * len(X))])
    y_train, y_test = np.split(y, [int(.8 * len(y))])

    return X_train, y_train, X_test, y_test

def main():

    train_set = []
    train_ground_truth = []
    test_set = []
    test_set_ground_truth = []

    groups = data_preprocessing()

    for key, group in groups:

        lags, target = generate_features(group)
        flat_lags = np.array(lags).reshape(-1)
        flat_target = np.array(target).reshape(-1)

        X_train, y_train, X_test, y_test = split_into_train_test(flat_lags, flat_target)

        train_set.append(X_train)
        train_ground_truth.append(y_train)
        test_set.append(X_test)
        test_set_ground_truth.append(y_test)

    X_train = np.stack(train_set, -1)
    y_train = np.stack(train_ground_truth, -1)
    X_test = np.stack(test_set, -1)
    y_test = np.stack(test_set_ground_truth, -1)

    submodels = []
    for link in range(len(groups)):
        config = ConnectedLstmConfig("lstm_connected" + str(link))
        submodels.append(LstmModel("connected", config))

	connected_config = ConnectedLstmConfig("lstm_connected")
	connected_model = ConnectedLstmModel(connected_config, submodels)

    connected_model.train(X_train, y_train)
    preds = connected_model.predict(X_test)

    errors = []
    for link in range(y_test.shape[1]):
        errors.append(np.sqrt(np.mean((np.array(preds[:,link]) - np.array(y_test[:,link]))**2)))

    df = pd.DataFrame()
    df["connected_model"] = errors
    df.to_csv('../data/connected_model.csv', index=False)

if __name__ == "__main__":
    main()
