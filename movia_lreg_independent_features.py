from lstm_common import *

import pandas as pd
import numpy as np
from sklearn import preprocessing
from sklearn import linear_model

def data_preprocessing():

    data = pd.read_csv('../data/4A_201701_Consistent.csv', sep=';')

    # Initial data-slicing
    data = data[(data.LinkTravelTime > 0) & (data.LineDirectionCode == 1)]

    # Data convertion
    data['DateTime'] = pd.to_datetime(data['DateTime'])
    time = pd.DatetimeIndex(data['DateTime'])
    data['TimeOfDayClass'] = 'NO_PEEK'
    data['Hour'] = time.hour
    data.ix[((7 < time.hour) & (time.hour < 9) & (
        data['DayType'] == 1)), 'TimeOfDayClass'] = 'PEEK'
    data.ix[((15 < time.hour) & (time.hour < 17) & (
        data['DayType'] == 1)), 'TimeOfDayClass'] = 'PEEK'

    data = data[(26 <= data.LineDirectionLinkOrder) &
                (data.LineDirectionLinkOrder <= 32)]

    grouping = data.groupby(['LinkRef'])

    return grouping

def generate_features(data):

    data = data.sort_values("DateTime", ascending=False)
    data = data.set_index(np.arange(0, data.shape[0], 1))

    num_lags = 20
    timeofday = []
    daytype = []
    target = []
    lags = []
    for i in range(data.shape[0]):
        lags.append(data.LinkTravelTime.shift(-1)[i:i + num_lags].values)
        target.append(data.iloc[i].LinkTravelTime)
        timeofday.append(data.iloc[i].TimeOfDayClass)
        daytype.append(data.iloc[i].DayType)

    lags = pd.DataFrame(lags)
    timeofday = pd.get_dummies(timeofday)
    daytype = pd.get_dummies(daytype)
    data = pd.concat([lags, timeofday, daytype], axis=1)

    data = np.array(data.dropna(axis=0))
    X = data[0:5000, :]
    y = np.array(target[0:5000]).reshape(-1, 1)

    assert X.shape[0] == y.shape[0]

    X = np.reshape(X, (X.shape[0], X.shape[1], 1))

    return X, y

def split_into_train_test(X, y):

    X_train, X_test = np.split(X, [int(.8 * len(X))])
    y_train, y_test = np.split(y, [int(.8 * len(y))])

    return X_train, y_train, X_test, y_test

def main():

    train_set = []
    train_ground_truth = []
    test_set = []
    test_set_ground_truth = []

    groups = data_preprocessing()

    for key, group in groups:

        lags, target = generate_features(group)
        X_train, y_train, X_test, y_test = split_into_train_test(lags, target)

        train_set.append(X_train)
        train_ground_truth.append(y_train)
        test_set.append(X_test)
        test_set_ground_truth.append(y_test)

    errors = []
    for link in range(len(groups)):

        regr = linear_model.LinearRegression()
        regr.fit(train_set[link][:, :, 0], train_ground_truth[link])
        preds = regr.predict(test_set[link][:,:,0])
        errors.append(np.sqrt(np.mean((np.array(preds) - np.array(test_set_ground_truth[link]))**2)))

    df = pd.DataFrame()
    df["independent_model"] = errors
    df.to_csv('../data/movia_lreg_independent_features.csv', index=False)


if __name__ == "__main__":
    main()
