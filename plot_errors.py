import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from tabulate import tabulate

def load_data():

    # load files with errors from single output models
    EWR_SO = pd.read_csv('data_report/SO_lstm_EWR_window.csv', sep="\t")
    JFK_SO = pd.read_csv('data_report/SO_lstm_JFK_window.csv', sep="\t")
    LGA_SO = pd.read_csv('data_report/SO_lstm_LGA_window.csv', sep="\t")

    # load files with errors from multi output models
    EWR_MO = pd.read_csv('data_report/MO_EWR_window.csv', sep="\t")
    JFK_MO = pd.read_csv('data_report/MO_JFK_window.csv', sep="\t")
    LGA_MO = pd.read_csv('data_report/MO_LGA_window.csv', sep="\t")

    # load files with overall errors for the various approaches

    OV_EWR_SO = pd.read_csv('data_report/SO_lstm_EWR_overall.csv', sep="\t")
    OV_JFK_SO = pd.read_csv('data_report/SO_lstm_JFK_overall.csv', sep="\t")
    OV_LGA_SO = pd.read_csv('data_report/SO_lstm_LGA_overall.csv', sep="\t")

    OV_MO = pd.read_csv('data_report/MO_overall.csv', sep="\t")

    return (EWR_SO, JFK_SO, LGA_SO, EWR_MO, JFK_MO, LGA_MO,
            OV_EWR_SO, OV_JFK_SO, OV_LGA_SO, OV_MO)


def show_errors(SO, MO, title):
    """ Show errors on a 5 day step window
    """
    fig = plt.figure(figsize=(15, 5))

    lstm_single, = plt.plot(SO['lstm'].values,
                            color="red", linewidth=1, label="LSTM Single Output")
    lstm_multiple, = plt.plot(
        MO['lstm'].values, color="blue", linewidth=1, label="LSTM Multi Output")

    lr_single, = plt.plot(SO['lr'].values, color="black",
                          linewidth=1, label="LinearReg Single Output")

    lr_multiple, = plt.plot(MO['lr'].values, color="green",
                            linewidth=1, label="LinearReg Multi Output")

    svr_single, = plt.plot(SO['svr'].values, color="gray",
                           linewidth=1, label="SVR Single Output")

    svr_multiple, = plt.plot(MO['svr'].values, color="orange",
                             linewidth=1, label="SVR Multi Output")

    plt.legend(handles=[lstm_single, lstm_multiple,
                        lr_single, lr_multiple, svr_single, svr_multiple])

    plt.xlabel('training steps')
    plt.ylabel('RMSE')
    plt.title(title)

    fig.savefig('plots/' + title + '.png')

def main():

    (EWR_SO, JFK_SO, LGA_SO, EWR_MO, JFK_MO, LGA_MO,
     OV_EWR_SO, OV_JFK_SO, OV_LGA_SO, OV_MO) = load_data()

    show_errors(EWR_SO, EWR_MO, "EWR")
    show_errors(JFK_SO, JFK_MO, "JFK")
    show_errors(LGA_SO, LGA_MO, "LGA")

if __name__ == "__main__":
    main()
